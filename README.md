## 4k minimum size hello_world - cross-compile targets: AArch32
(other targets are temporary removed or commented out - to read CMakeLists.txt less)

Visit: [arm-gnu-toolchain-downloads](https://developer.arm.com/downloads/-/arm-gnu-toolchain-downloads)

I used - 'arm-gnu-toolchain-13.2.rel1-x86_64-arm-none-linux-gnueabihf' (CTRL + F)
[arm-gnu-toolchain-downloads/13-2-rel1](https://developer.arm.com/downloads/-/arm-gnu-toolchain-downloads/13-2-rel1)


## build instruction - example of passing toolchain and sysroot via option:
```
cmake -E make_directory build
cd build
cmake ../ -D AARCH32_TOOLCHAIN="path_to/toolchain" -D AARCH32_SYSROOT="path_to/arm-none-linux-gnueabihf/libc"
cmake --build .
```

#### NOTE: to validate options check 'compile_commands.json' in build dir.

## arm/aarch32 libc 2.32 .so libc "compatibility"
```
$ strings ./.3rdparty/aarch32_glibc_2.32.so | grep ^GLIBC
GLIBC_2.4
GLIBC_2.5
GLIBC_2.6
GLIBC_2.7
GLIBC_2.8
GLIBC_2.9
GLIBC_2.10
GLIBC_2.11
GLIBC_2.12
GLIBC_2.13
GLIBC_2.14
GLIBC_2.15
GLIBC_2.16
GLIBC_2.17
GLIBC_2.18
GLIBC_2.22
GLIBC_2.23
GLIBC_2.24
GLIBC_2.25
GLIBC_2.26
GLIBC_2.27
GLIBC_2.28
GLIBC_2.29
GLIBC_2.30
GLIBC_2.32
GLIBC_PRIVATE
```
