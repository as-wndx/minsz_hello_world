# download x86_64 Linux hosted cross toolchain
https://developer.arm.com/downloads/-/arm-gnu-toolchain-downloads

https://clang.llvm.org/docs/CrossCompilation.html

# How to Cross Compile Compiler-rt Builtins For Arm
https://llvm.org/docs/HowToCrossCompileBuiltinsOnArm.html
https://llvm.org/docs/HowToCrossCompileBuiltinsOnArm.html#aarch64-target

https://en.wikipedia.org/wiki/ARM_architecture_family

# Cross Compiling With CMake
https://cmake.org/cmake/help/book/mastering-cmake/chapter/Cross%20Compiling%20With%20CMake.html

# cxx-cross-clang
https://mcilloni.ovh/2021/02/09/cxx-cross-clang/

# x32 is an ABI for amd64/x86_64 CPUs using 32-bit integers, longs and pointers.
https://stackoverflow.com/questions/7635013/difference-between-x86-x32-and-x64-architectures

https://www.google.com/search?q=smallest+executable+size+of+c+program

https://stackoverflow.com/questions/73144043/how-small-can-i-make-a-clang-executable

https://maskray.me/blog/2022-08-28-march-mcpu-mtune

https://stackoverflow.com/questions/65209344/investigating-the-size-of-an-extremely-small-c-program

https://www.muppetlabs.com/~breadbox/software/tiny/teensy.html

https://www.reddit.com/r/C_Programming/comments/wdag9l/how_to_absolutely_minimize_the_executable/

https://discourse.llvm.org/t/compiling-arm32-with-clang-no-available-targets-are-compatible-with-triple-armv6kz-unknown-linux-gnueabihf/64518

https://github.com/xaionaro-go/tinyhelloworld

https://montcs.bloomu.edu/Information/LowLevel/Assembly/hello-asm.html

https://gist.github.com/PotatoMaster101/1ddf8506a029fc39658bb330f1a12ec8

https://stackoverflow.com/questions/34758769/load-warning-cannot-find-entry-symbol-start
