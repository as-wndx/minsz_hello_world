#!/bin/sh
## build & run test.
## NOTE: manually export following environment variables
## to build/test under multiple compilers before submitting code!
#
# export CC=gcc   CXX=g++
# export CC=clang CXX=clang++
#
## build & run:
# ./scripts/build.sh
## build & run:
# ./scripts/build.sh
## make clean build & run:
# ./scripts/build.sh clean
## make cleaner build & run (for edge cases):
# ./scripts/build.sh cleaner

set -e

OLDIFS="$IFS"
NL='
' # New Line

LIBRARY_PATH="$HOME/Downloads/git/top/llvm-project/install/lib"
C_INCLUDE_PATH="$HOME/Downloads/git/top/llvm-project/install/include"
CPLUS_INCLUDE_PATH="$HOME/Downloads/git/top/llvm-project/install/include"

export LIBRARY_PATH
export C_INCLUDE_PATH
export CPLUS_INCLUDE_PATH


if [ ! -r ./CMakeLists.txt ]; then
    printf "%s\n" "Current directory does not contain CMakeLists.txt, exit."
    exit 67
fi

proj_dir=$(realpath ./)

bt="${BUILD_TYPE:-Release}"
echo "\$BUILD_TYPE=$bt"
getconf GNU_LIBC_VERSION
compiler="${CC:-_}"
## get compiler basename in case declared via full path
cmbn=$(basename "$compiler")
bdir="${proj_dir}/build/dev-$bt-$cmbn"
[ -d "$bdir" ] || mkdir -p "$bdir"

[ -n "$CTEST_OUTPUT_ON_FAILURE" ] || export CTEST_OUTPUT_ON_FAILURE=1

opt="$1"
clean_first=""
case "$opt" in
    clean|c)
        opt="$2"
        clean_first="--clean-first"
    ;;
    cleaner|cc) # for edge cases like corrupted GCDA files etc.
        opt="$2"
        [ -d "$bdir" ] && rm -rf "$bdir"
        mkdir -p "$bdir"
    ;;
esac

und='=========================='
sep="${und}${und}${und}"
vsep() {
    printf "\n%s[%s]\n%s\n\n" "${2}" "${1}" "${sep}${END}"
}

## NOTE: old cmake does not have options: [-S|-B|--fresh]
vsep "CONFIGURE" "${BLU}"
cd "$bdir" || exit 68
## NOTE: -Wno-error=dev => CMake complains that 3.0.0 is deprecated!
cmake -DCMAKE_BUILD_TYPE="$bt" \
-D WNDX_HELLO_USE_FPUTS=0 \
-D WNDX_HELLO_NOSTDLIB=0 \
-Wdev -Wno-error=dev "$proj_dir"

vsep "BUILD" "${CYN}"
cmake --build . ${clean_first}

# cmake --build . --target make_package

vsep "TESTS" "${MAG}"

bins="./minsz_hello_world_"

wc -c  "${bins}"*
echo # extra empty line
ls -lh --time-style=full-iso "${bins}"*
echo # extra empty line
file   "${bins}"*
echo # extra empty line
# size -A "${bins}"*
# echo # extra empty line


set +e # disable exitting on error temporarily

## run & check return code of our program: not segfaults, prints correctly, etc.
# fpaths=$(find . -path "${bins}*" -type f -executable)
# IFS="$NL"
# i=0
# for fpath in $fpaths; do
#     if "$fpath"; then # run
#         printf "%s%s%s" "${GRN}" "^ [ OK ](0)" "${END}"
#     else
#         our_prog_err_code="$?"
#         i=$((i+1))
#         printf "%s%s%s" "${RED}" "^ [FAIL]($our_prog_err_code)" "${END}"
#     fi
#     echo " $fpath"

# done
# IFS="$OLDIFS"
# [ "$i" -gt 0 ] && exit "$our_prog_err_code"


i=0
./minsz_hello_world_native || i=$((i+1))
# ./minsz_hello_world_x86_32 || i=$((i+1))
[ "$i" -gt 0 ] && exit "$i"

vsep "COMPLETED" "${GRN}"
