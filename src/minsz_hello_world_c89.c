/****************************************************************************
 * simple minimal size hello_world program written in plain C89.
 */

#ifndef WNDX_HELLO_NOSTDLIB
#define WNDX_HELLO_NOSTDLIB 0
#endif/*WNDX_HELLO_NOSTDLIB*/

#ifndef WNDX_HELLO_USE_FPUTS
#define WNDX_HELLO_USE_FPUTS 0
#endif/*WNDX_HELLO_USE_FPUTS*/


#if WNDX_HELLO_USE_FPUTS
typedef struct _IO_FILE FILE;
extern FILE *stdout;
extern int fputs (const char *__restrict __s, FILE *__restrict __stream);
#else
extern int puts (const char *__s);
#endif/*WNDX_HELLO_USE_FPUTS*/

#if WNDX_HELLO_NOSTDLIB
extern int _start(void);
int _start(void) {
#else
int main(void) {
#endif/*WNDX_HELLO_NOSTDLIB*/
#if WNDX_HELLO_USE_FPUTS
    fputs("hello world\n", stdout); /* with extra newline */
#else
    puts("hello world"); /* extra newline added by itself */
#endif/*WNDX_HELLO_USE_FPUTS*/
    return 0;
}
